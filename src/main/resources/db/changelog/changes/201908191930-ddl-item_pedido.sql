create table if not exists item_pedido (
       desconto double,
        preco double,
        quantidade integer,
        pedido_id integer not null,
        produto_id integer not null,
        primary key (pedido_id, produto_id)
);
