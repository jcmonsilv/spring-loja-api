CREATE TABLE IF NOT EXISTS PRODUTO (
	ID INTEGER AUTO_INCREMENT PRIMARY KEY,
	NOME VARCHAR(255),
	PRECO DECIMAL
);