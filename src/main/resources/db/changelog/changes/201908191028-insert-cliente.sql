INSERT INTO CLIENTE ( NOME, EMAIL, CPF_OU_CNPJ, TIPO )
	VALUES ( 'Cristiano Silva', 'cliente@email.com', '111111111111', 1);

INSERT INTO CLIENTE ( NOME, EMAIL, CPF_OU_CNPJ, TIPO )
	VALUES ( 'Jerry Monteiro', 'jerry@corp.com', '111111111111', 2);

INSERT INTO TELEFONE ( CLIENTE_ID, TELEFONES ) VALUES ( 1, '(12)33332222' );
INSERT INTO TELEFONE ( CLIENTE_ID, TELEFONES ) VALUES ( 1, '(12)991919191' );

INSERT INTO ENDERECO ( BAIRRO, CEP, COMPLEMENTO, LOGRADOURO, NUMERO, CIDADE_ID, CLIENTE_ID )
	VALUES ( 'Bairro da Cidade', '12000000', 'Casa', 'Rua Joaquim Silva', '12', 1, 1 );

INSERT INTO ENDERECO ( BAIRRO, CEP, COMPLEMENTO, LOGRADOURO, NUMERO, CIDADE_ID, CLIENTE_ID )
	VALUES ( 'Novo Bairro', '12000300', 'Apto', 'Rua Vassouras', '12A', 1, 1 );
