package br.com.ng5.jcmonsilv.puc.lojaapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.Cidade;

@Repository
public interface EstadoRepository extends JpaRepository<Cidade, Integer> {

}
