package br.com.ng5.jcmonsilv.puc.lojaapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ng5.jcmonsilv.puc.lojaapi.dto.ClienteDTO;
import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.Cliente;
import br.com.ng5.jcmonsilv.puc.lojaapi.repository.ClienteRepository;

@RestController
@RequestMapping(value="/clientes")
public class ClienteController {
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	private final String cacheCliente = "listaDeClientes";
	
	@GetMapping
	@Cacheable(value = cacheCliente)
	public Page<ClienteDTO> listar(
			@PageableDefault(sort = "id", direction = Direction.DESC, page = 0, size = 10) Pageable paginacao) {
		Page<Cliente> clientes = clienteRepository.findAll(paginacao);
		return ClienteDTO.converter(clientes);
	}
}
