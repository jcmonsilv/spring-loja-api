package br.com.ng5.jcmonsilv.puc.lojaapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.Estado;

@Repository
public interface CidadeRepository extends JpaRepository<Estado, Integer> {

}
