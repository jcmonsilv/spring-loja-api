package br.com.ng5.jcmonsilv.puc.lojaapi.dto;

import java.util.List;

import org.springframework.data.domain.Page;

import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.Categoria;
import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.Produto;

public class CategoriaDetalhesDTO {
	private Integer id;
	private String nome;
	private List<Produto> produto;

	public CategoriaDetalhesDTO(Categoria categoria) {
		this.id = categoria.getId();
		this.nome = categoria.getNome();
		this.produto = categoria.getProdutos();
	}

	public Integer getId() {
		return this.id;
	}

	public String getNome() {
		return this.nome;
	}
	
	public List<Produto> getProdutos(){
		return this.produto;
	}

	public static Page<CategoriaDetalhesDTO> converter(Page<Categoria> categorias) {
		return categorias.map(CategoriaDetalhesDTO::new);
	}
}
