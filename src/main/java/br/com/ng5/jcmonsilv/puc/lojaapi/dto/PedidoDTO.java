package br.com.ng5.jcmonsilv.puc.lojaapi.dto;

import java.util.Date;

import org.springframework.data.domain.Page;

import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.Pedido;

public class PedidoDTO {
	private Integer id;
	private Date data;
	private ClienteDTO cliente;

	public PedidoDTO(Pedido pedido) {
		this.id = pedido.getId();
		this.data = pedido.getInstante();
		this.cliente = new ClienteDTO(pedido.getCliente());
	}

	public Integer getId() {
		return id;
	}

	public Date getData() {
		return data;
	}

	public ClienteDTO getCliente() {
		return cliente;
	}

	public static Page<PedidoDTO> converter(Page<Pedido> pedidos) {
		return pedidos.map(PedidoDTO::new);
	}

}
