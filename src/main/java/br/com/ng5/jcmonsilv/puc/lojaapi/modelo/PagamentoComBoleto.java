package br.com.ng5.jcmonsilv.puc.lojaapi.modelo;

import java.util.Date;

import javax.persistence.Entity;

import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.enums.EstadoPagamento;

@Entity
public class PagamentoComBoleto extends Pagamento {
	private static final long serialVersionUID = 1L;
	private Date vencimento;
	private Date pagamento;

	public PagamentoComBoleto() {
	}

	public PagamentoComBoleto(EstadoPagamento estado, Pedido pedido, Date vencimento, Date pagamento) {
		super(estado, pedido);
		this.vencimento = vencimento;
		this.pagamento = pagamento;
	}

	public Date getVencimento() {
		return vencimento;
	}

	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}

	public Date getPagamento() {
		return pagamento;
	}

	public void setPagamento(Date pagamento) {
		this.pagamento = pagamento;
	}

}
