package br.com.ng5.jcmonsilv.puc.lojaapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableCaching
@EnableSwagger2
public class LojaApiApplication{	

	public static void main(String[] args) {
		SpringApplication.run(LojaApiApplication.class, args);
	}
}
