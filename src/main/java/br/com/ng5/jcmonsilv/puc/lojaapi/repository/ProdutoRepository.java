package br.com.ng5.jcmonsilv.puc.lojaapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.Produto;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Integer> {

}
