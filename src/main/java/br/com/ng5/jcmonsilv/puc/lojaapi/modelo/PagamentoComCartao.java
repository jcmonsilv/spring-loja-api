package br.com.ng5.jcmonsilv.puc.lojaapi.modelo;

import javax.persistence.Entity;

import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.enums.EstadoPagamento;

@Entity
public class PagamentoComCartao extends Pagamento {
	private static final long serialVersionUID = 1L;
	private Integer parcelas;

	public PagamentoComCartao() {
	}

	public PagamentoComCartao(EstadoPagamento estado, Pedido pedido, Integer parcelas) {
		super(estado, pedido);
		this.parcelas = parcelas;
	}

	public Integer getParcelas() {
		return parcelas;
	}

	public void setParcelas(Integer parcelas) {
		this.parcelas = parcelas;
	}
}
