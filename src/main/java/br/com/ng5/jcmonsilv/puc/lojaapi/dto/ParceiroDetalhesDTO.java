package br.com.ng5.jcmonsilv.puc.lojaapi.dto;

import java.util.List;

import org.springframework.data.domain.Page;

import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.Parceiro;
import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.Produto;

public class ParceiroDetalhesDTO {
	private Integer id;
	private String nome;
	private List<Produto> produto;

	public ParceiroDetalhesDTO(Parceiro parceiro) {
		this.id = parceiro.getId();
		this.nome = parceiro.getNome();
		this.produto = parceiro.getProdutos();
	}

	public Integer getId() {
		return this.id;
	}

	public String getNome() {
		return this.nome;
	}
	
	public List<Produto> getProdutos(){
		return this.produto;
	}

	public static Page<ParceiroDetalhesDTO> converter(Page<Parceiro> parceiros) {
		return parceiros.map(ParceiroDetalhesDTO::new);
	}
}
