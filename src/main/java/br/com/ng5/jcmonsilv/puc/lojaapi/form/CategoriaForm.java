package br.com.ng5.jcmonsilv.puc.lojaapi.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.Categoria;
import br.com.ng5.jcmonsilv.puc.lojaapi.repository.CategoriaRepository;

public class CategoriaForm {
	@NotNull
	@NotEmpty
	@Length(min=3)
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Categoria converter() {
		return new Categoria(this.nome);
	}

	public Categoria atualizar(Integer id, CategoriaRepository categoriaRepository) {
		Categoria categoria = categoriaRepository.getOne(id);
		categoria.setNome(this.nome);
		return categoria;
	}
	
	
}
