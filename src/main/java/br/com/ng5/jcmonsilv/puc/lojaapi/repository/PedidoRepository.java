package br.com.ng5.jcmonsilv.puc.lojaapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.Pedido;

public interface PedidoRepository extends JpaRepository<Pedido, Integer>{

}
