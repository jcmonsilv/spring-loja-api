package br.com.ng5.jcmonsilv.puc.lojaapi.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ng5.jcmonsilv.puc.lojaapi.dto.ParceiroDTO;
import br.com.ng5.jcmonsilv.puc.lojaapi.dto.ParceiroDetalhesDTO;
import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.Parceiro;
import br.com.ng5.jcmonsilv.puc.lojaapi.repository.ParceiroRepository;

@RestController
@RequestMapping(value = "/parceiros")
public class ParceiroController {

	@Autowired
	private ParceiroRepository parceiroRepository;

	private final String cacheParceiro = "listaDeParceiros";

	@GetMapping
	@Cacheable(value = cacheParceiro)
	public Page<ParceiroDTO> listar(
			@PageableDefault(sort = "nome", direction = Direction.ASC, page = 0, size = 10) Pageable paginacao) {
		Page<Parceiro> parceiros = parceiroRepository.findAll(paginacao);
		return ParceiroDTO.converter(parceiros);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<ParceiroDetalhesDTO> detalhar(@PathVariable("id") Integer id) {
		Optional<Parceiro> parceiro = parceiroRepository.findById(id);
		if (parceiro.isPresent()) {
			return ResponseEntity.ok(new ParceiroDetalhesDTO(parceiro.get()));
		}
		return ResponseEntity.notFound().build();
	}
}
