package br.com.ng5.jcmonsilv.puc.lojaapi.dto;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.Produto;

public class ProdutoDTO {

	private String nome;
	private Double preco;
	private List<String> categoria;
	private String parceiro;

	public ProdutoDTO(Produto produto) {
		this.nome = produto.getNome();
		this.preco = produto.getPreco();
		this.categoria = produto.getCategorias().stream().map(x -> x.getNome()).collect(Collectors.toList());
		this.parceiro = produto.getParceiro().getNome();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	public List<String> getCategoria() {
		return categoria;
	}

	public void setCategoria(List<String> categoria) {
		this.categoria = categoria;
	}

	public String getParceiro() {
		return parceiro;
	}

	public void setParceiro(String parceiro) {
		this.parceiro = parceiro;
	}

	public static Page<ProdutoDTO> converter(Page<Produto> produtos) {
		return produtos.map(ProdutoDTO::new);
	}

}
