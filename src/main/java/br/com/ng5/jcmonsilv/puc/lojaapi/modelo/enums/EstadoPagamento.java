package br.com.ng5.jcmonsilv.puc.lojaapi.modelo.enums;

public enum EstadoPagamento {	
	PENDENTE(1, "Pendente"),
	QUITADO(2, "Efetuado"),
	CANCELADO(3, "Cancelado");
	
	private int codigo;
	private String descricao;

	private EstadoPagamento(int codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public int getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public static EstadoPagamento toEnum(Integer codigo) {
		for (EstadoPagamento controle : EstadoPagamento.values()) {
			if (codigo.equals(controle.getCodigo())) {
				return controle;
			}
		}
		return null;
	}

}
