package br.com.ng5.jcmonsilv.puc.lojaapi.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.ng5.jcmonsilv.puc.lojaapi.dto.PedidoDTO;
import br.com.ng5.jcmonsilv.puc.lojaapi.dto.PedidoDetalhesDTO;
import br.com.ng5.jcmonsilv.puc.lojaapi.form.ItemDePedidoForm;
import br.com.ng5.jcmonsilv.puc.lojaapi.form.OrderDePedidoForm;
import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.Pedido;
import br.com.ng5.jcmonsilv.puc.lojaapi.repository.PedidoRepository;

@RestController
@RequestMapping(value="/pedidos")
public class PedidoController {
	
	@Autowired
	private PedidoRepository pedidoRepository;
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Value("${activemq.queue.pedido}")
	private String pedidoQueue;
	
	private final String cachePedidos = "listaPedidos";
	
	@GetMapping
	@Cacheable(value = cachePedidos)
	public Page<PedidoDTO> listar(
			@PageableDefault(sort = "id", direction = Direction.ASC, page = 0, size = 10) Pageable paginacao) {
		Page<Pedido> pedidos = pedidoRepository.findAll(paginacao);
		jmsTemplate.convertAndSend(pedidoQueue, "{data:'teste'}");
		return PedidoDTO.converter(pedidos);
	}
	
	@PostMapping
	@Transactional
	@CacheEvict(value=cachePedidos, allEntries=true)
	public ResponseEntity<Pedido> orderDePedido(@RequestBody OrderDePedidoForm form, 
			UriComponentsBuilder uriBuilder){		
		for (ItemDePedidoForm controle : form.getItens()) {
			System.out.println(controle.getProduto());
		}
		URI uri = uriBuilder.path("/pedidos/{id}").buildAndExpand(1).toUri();
		Optional<Pedido> pedido = pedidoRepository.findById(1);
		return ResponseEntity.created(uri).body(pedido.get());
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<PedidoDetalhesDTO> detalhar(@PathVariable("id") Integer id) {
		Optional<Pedido> pedido = pedidoRepository.findById(id);
		if (pedido.isPresent()) {
			return ResponseEntity.ok(new PedidoDetalhesDTO(pedido.get()));
		}
		return ResponseEntity.notFound().build();
	}
}
