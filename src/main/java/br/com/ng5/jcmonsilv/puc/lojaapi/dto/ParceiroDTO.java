package br.com.ng5.jcmonsilv.puc.lojaapi.dto;

import org.springframework.data.domain.Page;

import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.Parceiro;

public class ParceiroDTO {
	private Integer id;
	private String nome;	

	public ParceiroDTO(Parceiro parceiro) {
		this.id = parceiro.getId();
		this.nome = parceiro.getNome();
	}

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public static Page<ParceiroDTO> converter(Page<Parceiro> parceiros) {		
		return parceiros.map(ParceiroDTO::new);
	}	
}
