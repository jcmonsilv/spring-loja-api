package br.com.ng5.jcmonsilv.puc.lojaapi.form;

public class ItemDePedidoForm {
	private Integer produto;
	private Integer quantidade;

	public Integer getProduto() {
		return produto;
	}

	public void setProduto(Integer pedido) {
		this.produto = pedido;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
}
