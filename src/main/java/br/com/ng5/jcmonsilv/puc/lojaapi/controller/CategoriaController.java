package br.com.ng5.jcmonsilv.puc.lojaapi.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.ng5.jcmonsilv.puc.lojaapi.dto.CategoriaDTO;
import br.com.ng5.jcmonsilv.puc.lojaapi.dto.CategoriaDetalhesDTO;
import br.com.ng5.jcmonsilv.puc.lojaapi.form.CategoriaForm;
import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.Categoria;
import br.com.ng5.jcmonsilv.puc.lojaapi.repository.CategoriaRepository;

@RestController
@RequestMapping(value = "/categorias")
public class CategoriaController {

	@Autowired
	private CategoriaRepository categoriaRepository;

	private final String cacheCategoria = "listaDeCategorias";

	@GetMapping
	@Cacheable(value = cacheCategoria)
	public Page<CategoriaDTO> listar(
			@PageableDefault(sort = "id", direction = Direction.DESC, page = 0, size = 10) Pageable paginacao) {
		Page<Categoria> categorias = categoriaRepository.findAll(paginacao);
		return CategoriaDTO.converter(categorias);
	}

	@PostMapping
	@Transactional
	@CacheEvict(value = cacheCategoria, allEntries=true)
	public ResponseEntity<CategoriaDTO> cadastrar(@RequestBody @Valid CategoriaForm form,
			UriComponentsBuilder uriBuilder) {
		Categoria categoria = form.converter();
		categoriaRepository.save(categoria);

		URI uri = uriBuilder.path("/categorias/{id}").buildAndExpand(categoria.getId()).toUri();

		return ResponseEntity.created(uri).body(new CategoriaDTO(categoria));
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<CategoriaDetalhesDTO> detalhar(@PathVariable("id") Integer id) {
		Optional<Categoria> categoria = categoriaRepository.findById(id);
		if (categoria.isPresent()) {
			return ResponseEntity.ok(new CategoriaDetalhesDTO(categoria.get()));
		}
		return ResponseEntity.notFound().build();
	}

	@PutMapping(value="/{id}")
	@Transactional
	@CacheEvict(value=cacheCategoria, allEntries=true)
	public ResponseEntity<CategoriaDTO> atualizar(@PathVariable("id") Integer id,
			@RequestBody @Valid CategoriaForm form) {
		Optional<Categoria> optional = categoriaRepository.findById(id);
		if (optional.isPresent()) {
			Categoria categoria = form.atualizar(id, categoriaRepository);
			return ResponseEntity.ok(new CategoriaDTO(categoria));
		}
		return ResponseEntity.notFound().build();
	}
	
	@DeleteMapping(value="/{id}")
	@Transactional
	@CacheEvict(value=cacheCategoria, allEntries=true)
	public ResponseEntity<?> remover(@PathVariable("id") Integer id){
		Optional<Categoria> optional = categoriaRepository.findById(id);
		if ( optional.isPresent() ) {
			categoriaRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}
}
