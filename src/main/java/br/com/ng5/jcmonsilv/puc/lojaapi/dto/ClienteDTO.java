package br.com.ng5.jcmonsilv.puc.lojaapi.dto;

import org.springframework.data.domain.Page;

import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.Cliente;

public class ClienteDTO {
	private String nome;
	private String cpfOuCnpj;
	private String tipo;

	public ClienteDTO(Cliente cliente) {
		this.nome = cliente.getNome();
		this.cpfOuCnpj = cliente.getCpfOuCnpj();
		this.tipo = cliente.getTipo().getDescricao();
	}

	public String getNome() {
		return nome;
	}

	public String getCpfOuCnpj() {
		return cpfOuCnpj;
	}

	public String getTipo() {
		return tipo;
	}

	public static Page<ClienteDTO> converter(Page<Cliente> clientes) {
		return clientes.map(ClienteDTO::new);
	}

}
