package br.com.ng5.jcmonsilv.puc.lojaapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ng5.jcmonsilv.puc.lojaapi.dto.ProdutoDTO;
import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.Produto;
import br.com.ng5.jcmonsilv.puc.lojaapi.repository.ProdutoRepository;

@RestController
@RequestMapping(value = "/produtos")
public class ProdutoController {

	@Autowired
	private ProdutoRepository produtoRepository;

	private final String cacheProduto = "listaDeProdutos";

	@GetMapping
	@Cacheable(value = cacheProduto)
	public Page<ProdutoDTO> listar(
			@PageableDefault(sort = "id", direction = Direction.DESC, page = 0, size = 10) Pageable paginacao) {
		Page<Produto> produtos = produtoRepository.findAll(paginacao);
		return ProdutoDTO.converter(produtos);
	}
}
