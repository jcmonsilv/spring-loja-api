package br.com.ng5.jcmonsilv.puc.lojaapi.dto;

import org.springframework.data.domain.Page;

import br.com.ng5.jcmonsilv.puc.lojaapi.modelo.Categoria;

public class CategoriaDTO {
	private Integer id;
	private String nome;

	public CategoriaDTO(Categoria categoria) {
		this.id = categoria.getId();
		this.nome = categoria.getNome();
	}

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public static Page<CategoriaDTO> converter(Page<Categoria> categorias) {
		return categorias.map(CategoriaDTO::new);
	}
}
