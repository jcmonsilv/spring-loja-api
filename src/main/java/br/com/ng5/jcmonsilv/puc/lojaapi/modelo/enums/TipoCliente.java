package br.com.ng5.jcmonsilv.puc.lojaapi.modelo.enums;

public enum TipoCliente {
	PESSOAFISICA(1, "Pessoa Física"), 
	PESSOAJURIDICA(2, "Pessoa Jurídica");

	private int codigo;
	private String descricao;

	private TipoCliente(int codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public int getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public static TipoCliente toEnum(Integer codigo) {
		for (TipoCliente controle : TipoCliente.values()) {
			if (codigo.equals(controle.getCodigo())) {
				return controle;
			}
		}
		return null;
	}

}
