package br.com.ng5.jcmonsilv.puc.lojaapi.form;

import java.util.List;

public class OrderDePedidoForm {

	private List<ItemDePedidoForm> itens;

	public List<ItemDePedidoForm> getItens() {
		return itens;
	}

	public void setItens(List<ItemDePedidoForm> itens) {
		this.itens = itens;
	}

}
